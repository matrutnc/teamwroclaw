#!/bin/bash

if ! systemctl is-active --quiet nginx
    then
        # Install MongoDB
		sudo apt-get install unzip
        sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
        echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
        sudo apt-get update
        sudo apt-get install -y mongodb-org unzip
        sudo sed -i "s/127.0.0.1/0.0.0.0/" /etc/mongod.conf
        sudo systemctl enable mongod && sudo systemctl start mongod
		curl -sL "https://teamwroclawwsa.blob.core.windows.net/tw-blob/db.zip?sv=2018-03-28&ss=b&srt=sco&sp=r&se=2019-04-20T16:10:03Z&st=2019-03-29T09:10:03Z&spr=https&sig=9TslNmlenM1IlpcBRFss8dVHjamPBkiRR1MEsaOFaX4%3D" -o db.zip
        unzip db.zip -d .
        cd
        chmod +x ./db/import.sh
mongoimport --host localhost --db webratings --collection heroes --type json --file ./db/heroes.json --jsonArray
mongoimport --host localhost --db webratings --collection ratings --type json --file ./db/ratings.json --jsonArray
mongoimport --host localhost --db webratings --collection sites --type json --file ./db/sites.json --jsonArray
    else
        echo "MongoDB already installed.."
fi
